import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

import './IconButton.css'

class IconButton extends Component {
    render() {
        return (
            <div>
                <Button className={"icon-button"}>  <FontAwesomeIcon icon={this.props.icon} /> </Button>

            </div>
        );
    }
}

export default IconButton;