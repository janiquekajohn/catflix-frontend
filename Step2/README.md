# README #

This folder contains all final code or the state your application should be in after all the tasks of Step two are completed. You should follow along with the instructor to get to this stage. If we move on to step three and you are still stuck, clone this repo to bring your codebase up to speed, at the beginning of Step Three

### What do we cover in Step 2? ###

* We make the main view Component
* We add and style the main area Title
* We add and style the main area Tab-bar and Main Button
* We use map to iterate over the icons to be added to the settings area 

