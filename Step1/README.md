# README #

This folder contains all final code or the state your application should be in after all the tasks of Step one are completed. You should follow along with the instructor to get to this stage. If we move on to step two and you are still stuck, clone this repo to bring your codebase up to speed.

### What do we cover in Step 1? ###

* Quickly bootstrapping react with create-react-app
* Installing and setting up reactstrap
* Housekeeping, small things such as header text, background CSS
* Install extension ReactJs code snippets
* Installing font-awesome
* Make our first component! Icon Buttons

