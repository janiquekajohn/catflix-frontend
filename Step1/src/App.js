import "./App.css";
// import 'bootstrap/dist/css/bootstrap.min.css';
import {  Navbar } from "react-bootstrap";
import IconButton from "./Components/IconButton";
import { faCog,faSignOutAlt, faUserAlt,faGlobeAmericas,faQuestionCircle } from '@fortawesome/free-solid-svg-icons'

function App() {
  return (
    <div className="App">
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand href="#home">CATFLIX BACK OFFICE</Navbar.Brand>
        
        <Navbar.Collapse className="justify-content-end"> 
        <div className="curve">
      <svg width="100" height="100" viewbox="0 0 100 100" preserveAspectRatio="none">
  <path d="M0,100s35.908-5.068,49.015-49.014A76.2,76.2,0,0,1,69.6,16.741,73.559,73.559,0,0,1,94,1c1.978-.761,3.97-1.425,6-2q0.5,50.5,1,101H0Z" fill="#FFFFFF" />
</svg>
      </div>  
      <div className="icons"> <IconButton icon={faQuestionCircle}/>
        <IconButton icon={faGlobeAmericas}/>
          <IconButton icon={faUserAlt}/>
          <IconButton icon={faSignOutAlt}/>
          <IconButton icon={faCog}/>
          </div>   
        </Navbar.Collapse>
      </Navbar>
      
      
      
      
    </div>
  );
}

export default App;
