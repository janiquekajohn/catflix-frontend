# README #

This folder contains all final code or the state your application should be in after all the tasks of Step Three are completed. You should follow along with the instructor to get to this stage. If we move on to Step Four and you are still stuck, clone this repo to bring your codebase up to speed, at the beginning of Step Four

### What do we cover in Step 3? ###

* Set up the Grid Component (Material UI) in the ContentList Component
* Read in fake data and display it in the Grid
* Change the Main Panel to a React Functional Component
* Temporarily disable the search bar calling a function on change


