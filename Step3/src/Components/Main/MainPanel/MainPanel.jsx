import React, { useState, useEffect } from 'react';
import ContentList from '../../ContentList/ContentList'
import SearchBar from '../../SearchBar/SearchBar'
import './MainPanel.css'
import {data} from '../../ContentList/fakeContentList.js' 


const MainPanel = () => {

     const [contentList, setContentList] = useState();

     const fetchData = async () => {
    return await fetch('http://localhost:8080/movie')
      .then(response => response.json())
      .then(data => {
          console.log(data)
         setContentList(data) 
       });}

useEffect( () => {fetchData()},[]);
    
        return (
            <div className={"main-panel"}>
               <SearchBar/>
               <br/>
               <ContentList contentList={contentList ?? data} />
            </div>
        )
    
}

export default MainPanel