import React, { Component } from 'react'
import {  Button } from "react-bootstrap";
import './Main.css'
import MainPanel from './MainPanel/MainPanel';
export default class Main extends Component {
    render() {
        return (
            <div className={"main"}>
            <div className={"main-title"}>
                CONTENT CATALOGUE
            </div>
            <div className={"tab-row"}>
                <div className={"links"}>
                <a href="/">All</a>
                <a href="/">Tv Shows</a>
                <a href="/">Movies</a>
                <a href="/">Clips</a>
                </div>
               <Button className={"add-new"}>+ ADD NEW</Button>
            </div>
            <MainPanel/>
            </div>
        )
    }
}
