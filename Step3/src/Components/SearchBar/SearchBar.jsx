import React from 'react'
import './SearchBar.css'

export default function SearchBar({keyword,setKeyword}) {
    return (
       <input className={"search-bar"} key="searchBar" value={keyword} placeholder={"Search"} onChange={(e)=> console.log(e.target.value)}/>
    )
}
