export const data = [
    {
        "id": 1,
        "title": "Justice League (2021)",
        "rating": 3.55,
        "genre": "Action, Adventure, Fantasy, Sci-Fi"
    },
    {
        "id": 2,
        "title": "Joker (2019)",
        "rating": 3.85,
        "genre": "Crime, Drama, Thriller"
    },
    {
        "id": 3,
        "title": "The Matrix",
        "rating": 4.35,
        "genre": "Action, Sci-Fi"
    },
    {
        "id": 4,
        "title": "The Matrix Reloaded",
        "rating": 3.6,
        "genre": "Action, Sci-Fi"
    },
    {
        "id": 5,
        "title": "John Wick",
        "rating": 3.7,
        "genre": "Action, Crime, Thriller"
    },
    {
        "id": 6,
        "title": "John Wick: Chapter 2",
        "rating": 3.75,
        "genre": "Action, Crime, Thriller"
    },
    {
        "id": 7,
        "title": "John Wick: Chapter 3 - Parabellum",
        "rating": 3.7,
        "genre": "Action, Crime, Thriller"
    },
    {
        "id": 8,
        "title": "Deadpool",
        "rating": 4.0,
        "genre": "Action, Adventure, Comedy, Sci-Fi"
    }
]