import React from "react";
import "./ContentList.css";
import { DataGrid } from "@material-ui/data-grid";

const columns = [
  { field: 'Title', headerName: 'Title', width: 250 },
  { field: 'Rating', headerName: 'Rating', width: 150 },
  { field: 'Genre', headerName: 'Genre', width: 200 },
];

 
export default function ContentList({contentList=[]}) {
    
    const rows = contentList.map((content,idx) => ({id: idx, Title: content.title, Rating: content.rating, Genre: content.genre})) 

  return (
    <>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={10}
        checkboxSelection
      /> 
    </>
  );
}
