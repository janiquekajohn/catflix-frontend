# README #

![Catlfix Complete](CatflixComplete.png)

This repo contains the source code for the Mediagnenix Master Class Application "Catflix".


### What is this repository for? ###

It's meant to give you a good deep dive into Front End development. If you want further practice, 
after the end of the class, try enabling the search functionality against the backend from the backend 
master class.

### Folder Structure ###

As time is limited during the actual masterclass, this project is broken down into various Steps of the application. 
This gives you and opportunity to "jump ahead" in case you get lost or stuck at anytime during the class. After the 
class, you may also challenge yourself by redoing the steps with might have given you the most trouble, or that you want more practice on.

For example, let's say you want to hone your skills learned during Step 2. I would encourage you to then start from
Step 1. You can do this in two ways. First clone the parent repo (catflix-frontend).

*  git checkout Step1 - this will fast-forward you to the last commit of Step 1 for example
*  cd into the Step1 Folder, which contains the all of the code after Step 1 has been completed

### Happy Hacking :)  ###

